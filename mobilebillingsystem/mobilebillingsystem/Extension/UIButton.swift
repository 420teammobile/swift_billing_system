//
//  UIButton.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func contentInsetGeneric(){
        self.contentEdgeInsets.left = Constants.styles.contentInsetLeft
        self.contentEdgeInsets.right = Constants.styles.contentInsetRight
        self.contentEdgeInsets.top = Constants.styles.contentInsetTop
        self.contentEdgeInsets.bottom =  Constants.styles.contentInsetBottom
    }
    
    func enable(){
        self.isEnabled = true
        self.backgroundColor = Constants.colors.principalColor
        self.setTitleColor(Constants.colors.white, for: .normal)
    }
    
    func toDisable(){
        self.isEnabled = false
        self.backgroundColor = Constants.colors.gray
        self.setTitleColor(Constants.colors.white, for: .normal)
    }
}
