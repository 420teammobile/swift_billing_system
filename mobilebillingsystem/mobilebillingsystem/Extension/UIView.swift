//
//  UIView.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func backgroundPrincipalColor(){
        self.backgroundColor = Constants.colors.principalColor
    }
}
