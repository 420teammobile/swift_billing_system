//
//  LoginController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class LoginController: BaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadHandlers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadStyleTabBarController(isHidden: true, title: "LOGIN")
    }
    
    //MARK: - Private functions 
    private func loadHandlers(){
        self.signInButton.addTarget(self, action: #selector(signInUser), for: UIControl.Event.touchUpInside)
        self.registerButton.addTarget(self, action: #selector(registerNewUser), for: UIControl.Event.touchUpInside)
    }
    
    //MARK: - Events functions
    //Aqui se debe autenticar el cliente
    @objc func signInUser(sender: UIButton){
        //Antes se debe validar si el usuario es valido para dejarlo seguir
        self.showStoreAvailables()
    }
    
    private func showStoreAvailables(){
        let storeAvailables = Controller.StoreAvailablesController()
        self.navigationController?.pushViewController(storeAvailables, animated: true)
    }
    
    //Se debe enviar a la vista de registro de nuevo usuario
    @objc func registerNewUser(sender: UIButton){
        let registerController = Controller.RegisterController()
        self.navigationController?.pushViewController(registerController, animated: true)
    }
}
