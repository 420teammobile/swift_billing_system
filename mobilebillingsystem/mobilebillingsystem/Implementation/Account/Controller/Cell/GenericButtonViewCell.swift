//
//  GenericButtonViewCell.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import UIKit

class GenericButtonViewCell: UITableViewCell {

    //MARK: - Key
    static let key = "GenericButtonViewCell"
    
    //MARK: - Completion Handlers
    var action : (() -> Void)?
    
    //MARK: - Outlets
    @IBOutlet weak var actionButton: UIButton!
    
    //MARK: - Override functions
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadHandlers()
    }
    
    private func loadHandlers() {
        self.actionButton.removeTarget(self, action: nil, for: .allEvents)
        self.actionButton.addTarget(self, action: #selector(showAction), for: .touchUpInside)
    }
    
    @objc func showAction(sender: UIButton){
        self.action?()
    }
}
