//
//  AccountController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import UIKit

class AccountController: BaseViewController {

    //MARK: - Attributes
    
    //MARK: - Properties
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadExternalViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.loadStyleTabBarController(isHidden: false, title: "MI CUENTA", returnOptionAvailable: true)
    }
    
    //MARK: - Private functions
    private func loadExternalViews(){
        self.tableView.register(UINib(nibName: GenericButtonViewCell.key, bundle: nil), forCellReuseIdentifier: GenericButtonViewCell.key)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    //MARK: - Public functions
}

extension AccountController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GenericButtonViewCell.key, for: indexPath) as? GenericButtonViewCell
        cell?.actionButton.setTitle("CERRAR SESIÓN", for: UIControl.State.normal)
        return cell!
    }
}
