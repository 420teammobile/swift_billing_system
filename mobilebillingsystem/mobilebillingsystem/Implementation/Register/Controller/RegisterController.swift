//
//  RegisterController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class RegisterController: BaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var FirtsNameTextField: UITextField!
    
    @IBOutlet weak var telephoneTextField: UITextField!
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadHandlers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadStyleTabBarController(isHidden: false, title: "REGISTRO")
    }
    
    //MARK: - Events functions
    private func loadHandlers(){
        self.registerButton.addTarget(self, action: #selector(registerUser), for: UIControl.Event.touchUpInside)
    }
    
    //Registro de nuevo usuario
    @objc func registerUser(sender: UIButton){
        //Una vez se valide la información y se registre el usuario se procede a pasarlo a la vista de
        //Tiendas disponibles
        self.showStoreAvailables()
    }
    
    private func showStoreAvailables(){
        let controller = Controller.StoreAvailablesController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
