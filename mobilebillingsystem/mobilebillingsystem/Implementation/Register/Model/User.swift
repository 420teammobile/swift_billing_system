//
//  User.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//
import Foundation

class User : Codable {
    
    var id : Int?
    var name : String?
    var password: String?
    var telephone : Int64?
    var user : String?
    
    init() {}
    
    init(name: String, password: String, telephone: Int64, user: String) {
        self.name = name
        self.password = password
        self.telephone = telephone
        self.user = user
    }
}
