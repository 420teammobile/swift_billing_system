//
//  Product.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class Product : Codable {
    
    var image : String?
    var description : String?
    var id : String?
    var name : String?
    var price : Int?
    var quantity : Int?
    var stock : Int?
    var isUpdatedStockNow : Bool? = false
}
