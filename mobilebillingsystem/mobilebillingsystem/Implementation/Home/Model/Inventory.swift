//
//  Inventory.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class Inventory : Codable {
    
    var idProduct : String?
    var idWarehouse : String?
    var quantity : Int?
}
