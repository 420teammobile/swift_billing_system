//
//  Store.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class Store : Codable {
    
    var description : String?
    var email : String?
    var id : String?
    var idWarehouse : String?
    var name : String?
    var urlLogo : String?
}
