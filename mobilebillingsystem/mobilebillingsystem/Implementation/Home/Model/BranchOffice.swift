//
//  BranchOffice.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class BranchOffice : Codable {
    
    var description : String?
    var id : String?
    var idStore : String?
    var name : String?
}
