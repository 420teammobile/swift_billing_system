//
//  ListProductsController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class ListProductsController: BaseViewController {
    
    //MARK: - Attributes
    var store: Store?
    var products : [Product] = [Product]()
    var productsToFilter : [Product] = [Product]()
    var isSearcherActived : Bool = false
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadExternalViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadStyleTabBarController(isHidden: false, title: "PRODUCTOS", returnOptionAvailable: true)
        self.getAllProducts()
    }
    
    //MARK: - Private functions
    private func loadExternalViews(){
        self.tableView.register(UINib(nibName: SearcherViewCell.key, bundle: nil), forCellReuseIdentifier: SearcherViewCell.key)
        self.tableView.register(UINib(nibName: ProductViewCell.key, bundle: nil), forCellReuseIdentifier: ProductViewCell.key)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func getAllProducts() {
        let dataBase = DataBaseFireBaseManager()
        dataBase.getAllProductsForStore(store: self.store!, completionHandler: { (products) in
            self.products = products
            self.tableView.reloadData()
            dataBase.detectChangedInInventory(products: self.products, store: self.store!) { (inventory) in
                if inventory != nil {
                    let listProductViewModel = ListProductViewModel()
                    listProductViewModel.updateStockInProductsFromStore(products: self.products, store: self.store!, inventory: inventory!) { (products) in
                        self.products = products
                        self.tableView.reloadData()
                    }
                }
            }
        })
    }
}

extension ListProductsController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            if self.isSearcherActived {
                return self.productsToFilter.count
            }
            return self.products.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: SearcherViewCell.key, for: indexPath) as? SearcherViewCell
            cell?.searcherChanged = searcherChanged(_:)
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductViewCell.key, for: indexPath) as? ProductViewCell
            var product : Product!
            if self.isSearcherActived {
                product = self.productsToFilter[indexPath.row]
            }else{
                product = self.products[indexPath.row]
            }
            cell?.loadProduct(product: product)
            cell?.changedValueProduct = changeValueFromProduct(_:_:)
            return cell!
        }
    }
    
    private func searcherChanged(_ textToSearch : String) {
        if textToSearch.isEmpty {
            self.isSearcherActived = false
        }else{
            self.productsToFilter =  products.filter { (product) -> Bool in
                self.isSearcherActived = true
                if (product.name?.uppercased().contains(textToSearch.uppercased()))! {
                    return true
                }
                return false
            }
        }
        let indexSet : IndexSet = IndexSet(arrayLiteral: 1)
        self.tableView.reloadSections(indexSet, with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*if self.products.count > 0 {
            let store = self.products[indexPath.row]
        }*/
    }
    
    private func changeValueFromProduct(_ product : Product,_ isAdeed : Bool){
        //Aqui se debe validar la disponibilidad en stock del producto
        var index : Int = 0
        if var productsPurchased = JSONManager().getCarToBuySaved() {
            var found : Bool = false
            var remove : Bool = false
            for productPurchased in productsPurchased {
                if productPurchased.idProduct == product.id {
                    found = true
                    productPurchased.quantity = product.quantity
                    if productPurchased.quantity == 0{
                        remove = true
                        found = false
                    }
                    break
                }
                index = index + 1
            }
            if !found {
                let productPurchased : ProductPurchased = ProductPurchased()
                productPurchased.idProduct = product.id
                productPurchased.quantity = product.quantity
                productsPurchased.append(productPurchased)
            }else if remove {
                productsPurchased.remove(at: index)
            }
            do {
                let jsonData = try JSONEncoder.init().encode(productsPurchased)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                JSONManager().saveJSONString(key: Constants.userDefault.orderPurchasing, json: jsonString)
                super.navigationBar.updateStatusCar()
            }catch{}
        }else {
            var productsPurchased : [ProductPurchased] = [ProductPurchased]()
            let productPurchased : ProductPurchased = ProductPurchased()
            productPurchased.idProduct = product.id
            productPurchased.quantity = product.quantity
            productPurchased.orderPurchase = DataBaseFireBaseManager().getNewOrderPurchase()
            productsPurchased.append(productPurchased)
            do {
                let jsonData = try JSONEncoder.init().encode(productsPurchased)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                JSONManager().saveJSONString(key: Constants.userDefault.orderPurchasing, json: jsonString)
            }catch{}
            super.navigationBar.updateStatusCar()
        }
    }
}

