//
//  StoreAvailablesController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class StoreAvailablesController: BaseViewController {

    //MARK: - Attributes
    var stores : [Store] = [Store]()
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadExternalViews()
        self.getAllStore()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadStyleTabBarController(isHidden: false, title: "TIENDAS", returnOptionAvailable: true)
    }
    
    //MARK: - Private functions
    private func loadExternalViews(){
        self.tableView.register(UINib(nibName: StoreViewCell.key, bundle: nil), forCellReuseIdentifier: StoreViewCell.key)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func getAllStore() {
        let dataBase = DataBaseFireBaseManager()
        dataBase.getAllStores { (stores) in
            self.stores = stores
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

extension StoreAvailablesController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StoreViewCell.key, for: indexPath) as? StoreViewCell
        let store = self.stores[indexPath.row]
        cell?.loadStore(store: store)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.stores.count > 0 {
            let store = self.stores[indexPath.row]
            self.showListProductFromStore(store: store)
        }
    }
    
    func showListProductFromStore(store: Store){
        let controller = Controller.ListProductsController()
        controller.store = store
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
