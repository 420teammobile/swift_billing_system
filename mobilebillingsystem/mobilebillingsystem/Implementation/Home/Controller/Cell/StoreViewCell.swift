//
//  StoreViewCell.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import UIKit
import SDWebImage

class StoreViewCell: UITableViewCell {
    
    //MARK: - Key
    static let key = "StoreViewCell"
    
    //MARK: - Outlets
    @IBOutlet weak var nameStoreLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var logoStoreImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!

    //MARK: - Override functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Public functions
    func loadStore(store : Store){
        nameStoreLabel.text = store.name
        descriptionLabel.text = store.description
        if !store.urlLogo!.isEmpty {
            logoStoreImageView.sd_setImage(with: URL(string: store.urlLogo!)) { (image, error, imageCacheType, url) in
            }
        }
    }
}
