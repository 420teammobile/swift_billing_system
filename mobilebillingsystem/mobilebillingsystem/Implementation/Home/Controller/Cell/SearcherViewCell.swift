//
//  SearcherViewCell.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import UIKit

class SearcherViewCell: UITableViewCell {
    
    //MARK: - Key
    static let key = "SearcherViewCell"
    
    //MARK: - CompletionHandlers
    
    var searcherChanged : ((_ textToSearch : String) -> Void)?
    
    //MARK: - Outlets
    @IBOutlet weak var searcherBar: UISearchBar!
    
    //MARK: - Override functions
    override func awakeFromNib() {
        super.awakeFromNib()
        searcherBar.delegate = self
    }
}

extension SearcherViewCell : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.searcherChanged?(searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.searcherChanged?("")
    }
}
