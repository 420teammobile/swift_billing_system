//
//  ProductViewCell.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import UIKit
import SDWebImage

class ProductViewCell: UITableViewCell {
    
    //MARK: - Key
    static let key = "ProductViewCell"
    
    //MARK: - Attributes and Properties
    var product : Product!
    var changedValueProduct : ((_ product : Product,_ isAdeed: Bool) -> Void)?
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var containerQuantitiesView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var quantityToAddLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    
     @IBOutlet weak var alertUnitExpiring: UILabel!
    
    //MARK: - Override functions
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadStyles()
        self.loadHandlers()
    }
    
    //MARK: - Private functions
    private func loadStyles(){
        self.containerQuantitiesView.layer.cornerRadius = 10
        self.containerQuantitiesView.layer.borderColor = Constants.colors.principalColor.cgColor
        self.containerQuantitiesView.layer.borderWidth = 1
    }
    
    private func loadHandlers(){
        self.minusButton.layer.cornerRadius = self.minusButton.frame.width / 2
        self.plusButton.layer.cornerRadius = self.plusButton.frame.width / 2
        self.minusButton.removeTarget(self, action: nil, for: .allEvents)
        self.minusButton.addTarget(self, action: #selector(minusQuantity), for: .touchUpInside)
        self.plusButton.removeTarget(self, action: nil, for: .allEvents)
        self.plusButton.addTarget(self, action: #selector(plusQuantity), for: .touchUpInside)
    }

    //MARK: - Public functions
    func loadProduct(product: Product){
        self.product = product
        nameLabel.text = product.name
        if let price = product.price {
            priceLabel.text =  "Precio: $\(Int(price).formattedWithSeparator) + 19% IVA"
        }else{
            priceLabel.text = ""
        }
        if let stock = product.stock {
            self.quantityLabel.text = "Disponibilidad: \(stock)"
            if product.isUpdatedStockNow != nil && product.isUpdatedStockNow! {
                UIView.animate(withDuration: TimeInterval(exactly: 2.0)!, animations: {
                    self.quantityLabel.font = UIFont.boldSystemFont(ofSize: 20)
                }) { (finish) in
                    self.quantityLabel.font = UIFont.systemFont(ofSize: 16)
                }
            }
            //Se debe cambiar por un valor configurable
            if stock < 10 {
                self.alertUnitExpiring.text = String(format: self.alertUnitExpiring.text!, String(stock))
                self.alertUnitExpiring.isHidden = false
            }else{
                self.alertUnitExpiring.isHidden = true
            }
        }else {
           quantityLabel.text = "No se pudo cargar el stock"
        }
        if let quantity = product.quantity {
            if let stock = product.stock, stock < quantity {
                self.quantityToAddLabel.text = "\(stock)"
                product.quantity = stock
                //Se debe notificar que se actualizo el valor porque el stock actual es inferior a la cantidad que tenia guardada
            }else{
                self.quantityToAddLabel.text = "\(quantity)"
            }
        }else{
           self.quantityToAddLabel.text = "0"
        }
        if product.image != nil && !product.image!.isEmpty {
            iconImageView.sd_setImage(with: URL(string: product.image!)) { (image, error, imagechaCacheType, url) in
            }
        }
    }
    
    //MARK: - Events functions
    @objc func minusQuantity(sender: UIButton) {
        if var quantityToAdd = Int(quantityToAddLabel.text!) {
            if quantityToAdd > 0 {
                quantityToAdd = quantityToAdd - 1
                quantityToAddLabel.text = "\(quantityToAdd)"
                self.product.quantity = quantityToAdd
                self.changedValueProduct?(self.product, false)
            }
        }
    }
    
    @objc func plusQuantity(sender: UIButton) {
        if var quantityToAdd = Int(self.quantityToAddLabel.text!) {
            quantityToAdd += 1
            if product.stock! >= quantityToAdd {
                self.quantityToAddLabel.text = "\(quantityToAdd)"
                self.product.quantity = quantityToAdd
                self.changedValueProduct?(self.product, true)
            }
        }
    }
}
