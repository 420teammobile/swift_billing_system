//
//  ListProductViewModel.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class ListProductViewModel {
    
    func updateStockInProductsFromStore(products: [Product], store: Store,  inventory: Inventory, completionHandler: @escaping ([Product]) -> Void){
        //Se debe actualizar la cantidad de productos en el stock si este cambia
        for product in products {
            if product.id == inventory.idProduct && inventory.idWarehouse == store.idWarehouse {
                product.stock = inventory.quantity
                break
            }
        }
        completionHandler(products)
    }
    
    func updateStockInProducts(products: [Product],  inventory: Inventory, completionHandler: @escaping ([Product]) -> Void){
        let productsPurchased = JSONManager().getCarToBuySaved()
        //Se debe actualizar la cantidad de productos en el stock si este cambia
        for product in products {
            if productsPurchased != nil {
                for productPurchased in productsPurchased! {
                    if product.id == inventory.idProduct && productPurchased.idProduct == product.id {
                        product.stock = inventory.quantity
                        if product.stock! < product.quantity! {
                            product.quantity = product.stock
                            product.isUpdatedStockNow = true
                            productPurchased.quantity = product.quantity
                        }
                        break
                    }
                }
            }else{
                if product.id == inventory.idProduct  {
                    product.stock = inventory.quantity
                    if product.stock! < product.quantity! {
                        product.quantity = product.stock
                        product.isUpdatedStockNow = true
                    }
                    break
                }
            }
            do {
                let jsonData = try JSONEncoder.init().encode(productsPurchased)
                let jsonString = String(data: jsonData, encoding: .utf8)!
                JSONManager().saveJSONString(key: Constants.userDefault.orderPurchasing, json: jsonString)
            }catch{}
        }
        completionHandler(products)
    }
}
