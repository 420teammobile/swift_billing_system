//
//  TabBarView.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class TabBarView: UIView {
    
    //MARK: - Identifier
    static let key  = "TabBarView"
    
    //MARK: - CompletionHandlers
    var openCarToBuySummary : (() -> Void)?
    var openAccountDetail : ((_ open: Bool) -> Void)?
    var goBackViewController : (() -> Void)?
    
    
    //MARK: Attributes
    @IBOutlet weak var returnIconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var returnActionButton: UIButton!
    
    
    @IBOutlet weak var totalProductsLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var carToBuyButton: UIButton!
    @IBOutlet weak var myAccountButton: UIButton!
    
    //MARK: Overrides Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadHandlers()
        self.loadStyles()
    }
    
    //MARK: Private Methods
    private func loadHandlers(){
        self.myAccountButton.removeTarget(self, action: nil, for: .allEvents)
        self.myAccountButton.addTarget(self, action: #selector(myAccountController), for: .touchUpInside)
        self.carToBuyButton.removeTarget(self, action: nil, for: .allEvents)
        self.carToBuyButton.addTarget(self, action: #selector(carToBuyController), for: .touchUpInside)
    }
    
    private func loadStyles(){
        totalProductsLabel.layer.cornerRadius = totalProductsLabel.frame.width / 2
    }

    //MARK: - Public Methods
    func updateStatusCar(){
        if let productsPurchased = JSONManager().getCarToBuySaved() {
            self.totalProductsLabel.text = "\(productsPurchased.count)"
            let dataBase = DataBaseFireBaseManager()
            dataBase.getAllProductsForStore(store: nil) { (products) in
                var totalPriceToBuy = 0.0
                for product in products {
                    for productPruchase in productsPurchased{
                        if product.id == productPruchase.idProduct {
                            //PENDIENTE: Se debe dejar configurable el porcentaje de iva
                            totalPriceToBuy = totalPriceToBuy + (Double(product.price!) * Double(productPruchase.quantity!) + (Double(product.price!) * 0.19))
                            break
                        }
                    }
                }
                self.totalPriceLabel.text = "\(Int(totalPriceToBuy).formattedWithSeparator)"
            }
        }else{
            self.totalProductsLabel.text = "0"
            self.totalPriceLabel.text = "$0"
        }
    }
    
    //MARK: - Events functions
    @objc func myAccountController(sender: UIButton){
        openAccountDetail?(true)
    }
    
    @objc func carToBuyController(sender: UIButton){
        openCarToBuySummary?()
    }
}

