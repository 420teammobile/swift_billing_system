//
//  CarToBuyViewModel.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class CarToBuyViewModel {
    
    func emptyCar(completionHandler: @escaping ([Product]) -> Void){
        JSONManager().saveJSONString(key: Constants.userDefault.orderPurchasing, json: "")
        completionHandler([Product]())
    }
    
    
    func getTotalProducts(completionHandler: @escaping ([Product]) -> Void){
        let dataBase = DataBaseFireBaseManager()
        dataBase.getAllProductsForStore(store: nil) {
            (products) in
            var productsResult = [Product]()
            if let productsPurchased = JSONManager().getCarToBuySaved() {
                if productsPurchased.count > 0 && products.count > 0 {
                    for productPurchase in productsPurchased {
                        var found : Bool = false
                        var index : Int = 0
                        for product in products {
                            if productPurchase.idProduct == product.id {
                                found = true
                                product.quantity = productPurchase.quantity
                                if product.quantity == 0 {
                                    found = false
                                }
                                else if product.stock! < product.quantity! {
                                    //Se debe informar al usuario que ya no se encuentra las unidades que iba a comprar
                                    product.quantity = product.stock
                                    productPurchase.quantity = product.quantity
                                }
                                if found {
                                    productsResult.append(product)
                                }
                                break
                            }
                            index = index + 1
                        }
                    }
                    do {
                        let jsonData = try JSONEncoder.init().encode(productsPurchased)
                        let jsonString = String(data: jsonData, encoding: .utf8)!
                        JSONManager().saveJSONString(key: Constants.userDefault.orderPurchasing, json: jsonString)
                    }catch{}
                }
            }else{
               productsResult = [Product]()
            }
            completionHandler(productsResult)
        }
    }
}
