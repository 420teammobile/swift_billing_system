//
//  CustomerPurchaseHistory.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class CustomerPurchaseHistory : Codable{
    var idUser : String?
    var orderPurchase : String?
}
