//
//  ProductPurchased.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class ProductPurchased : Codable {
    var idProduct : String?
    var orderPurchase : String?
    var quantity : Int?
}
