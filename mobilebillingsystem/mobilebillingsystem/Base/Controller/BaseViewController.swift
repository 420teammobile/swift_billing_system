//
//  BaseViewController.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    //MARK: - Attributes
    var navigationBar : TabBarView!
    
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadExternalViews()
    }
    
    //MARK: - Private functions
    private func LoadExternalViews(){
        if let navigationController = self.navigationController{
            Utility.LoadNavigationView(navigationBar: navigationController.navigationBar, title: "", controller: self)
        }
    }
    
    func loadStyleTabBarController(isHidden: Bool, title: String, returnOptionAvailable : Bool = true){
        //self.view.backgroundPrincipalColor()
        self.navigationController?.navigationBar.barStyle = Constants.styles.whiteBarStyle
        self.navigationController?.setNavigationBarHidden(isHidden, animated: true)
        if !isHidden {
            if let navigationItem = self.navigationController?.navigationBar.subviews[ (self.navigationController?.navigationBar.subviews.count)! - 1] as? TabBarView{
                self.navigationBar = navigationItem
                navigationItem.titleLabel.text = title
                navigationItem.openAccountDetail = self.openAccountProfile(_:)
                navigationItem.openCarToBuySummary = self.openCarToBuySummary
                navigationItem.returnActionButton.removeTarget(nil, action: nil, for: .allEvents)
                navigationItem.updateStatusCar()
                if returnOptionAvailable {
                    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                    navigationItem.returnIconImageView.isHidden = false
                    navigationItem.returnActionButton.addTarget(self, action: #selector(actionReturn), for: UIControl.Event.touchUpInside)
                }else{
                    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                    navigationItem.returnIconImageView.isHidden = true
                }
            }
        }
    }
    
    //MARK: - Events functions
    @objc func actionReturn(sender: UIButton){
        //Antes se debe validar si el usuario es valido para dejarlo seguir
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openCarToBuySummary(){
        let controller = Controller.CarToBuyController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func openAccountProfile(_ open: Bool){
        let controller = Controller.AccountController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
