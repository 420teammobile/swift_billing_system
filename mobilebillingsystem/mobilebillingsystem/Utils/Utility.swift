//
//  Utility.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    static func LoadNavigationView(navigationBar: UINavigationBar, title: String, controller : UIViewController){
        let bounds = controller.navigationController?.navigationBar.bounds
        let navigationView = Bundle.main.loadNibNamed(TabBarView.key, owner: controller, options: nil)?.first as? TabBarView
        let navigationBarViewFrame = CGRect(x: 0, y: 0, width: bounds!.width, height: bounds!.height)
        navigationView!.frame = navigationBarViewFrame
        navigationView!.titleLabel.text = title
        navigationBar.addSubview(navigationView!)
    }
}
