//
//  Controller.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import Foundation
import UIKit

class Controller {
    
    static func RegisterController() -> RegisterController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.registerController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.registerController) as! RegisterController
        return controller
    }
    
    static func CarToBuyController() -> CarToBuyController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.carToBuyController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.carToBuyController) as! CarToBuyController
        return controller
    }
    
    static func HomeController() -> HomeController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.homeController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.homeController) as! HomeController
        return controller
    }
    
    static func ListProductsController() -> ListProductsController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.listProductsController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.listProductsController) as! ListProductsController
        return controller
    }
    
    static func StoreAvailablesController() -> StoreAvailablesController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.storeAvailablesController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.storeAvailablesController) as! StoreAvailablesController
        return controller
    }
    
    static func LoginController() -> LoginController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.loginController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.loginController) as! LoginController
        return controller
    }
    
    static func AccountController() -> AccountController {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoardName.accountController, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoardControllerName.accountController) as! AccountController
        return controller
    }
}
