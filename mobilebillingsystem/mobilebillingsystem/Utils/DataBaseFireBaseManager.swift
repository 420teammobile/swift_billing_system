//
//  DataBaseFireBaseManager.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation
import FirebaseDatabase
import CodableFirebase

struct EntitiesNameFireBase {
    static let BranchOffice =  "BranchOffice"
    static let Customer =  "Customer"
    static let CustomerPurchaseHistory =  "CustomerPurchaseHistory"
    static let Inventory =  "Inventory"
    static let Product =  "Product"
    static let ProductPurchased =  "ProductPurchased"
    static let Provider =  "Provider"
    static let Settings =  "Settings"
    static let Store =  "Store"
    static let Warehouse =  "Warehouse"
}


class DataBaseFireBaseManager {
    
    var ref: DatabaseReference!
    var changed : (() -> Void)?
    
    init() {
        self.ref = Database.database().reference()
    }
    
    //Funcion para validar si un usuario existe y si es valida la autenticacion
    func existUser(username: String, password: String, completionHandler: @escaping (Bool, User?) -> Void){
        self.ref.child( EntitiesNameFireBase.Customer)
            .queryOrdered(byChild: "username")
            .queryEqual(toValue: username)
            .observeSingleEvent(of: .value, with: { (snapshot) in
                guard let value = snapshot.value else {
                    completionHandler(false, nil)
                    return
                }
                do {
                    let model = try FirebaseDecoder().decode(User.self, from: value)
                    completionHandler(true, model)
                } catch let error {
                    print(error)
                    completionHandler(false, nil)
                }
            })
    }
    
    //Funcion para devolver todas las tiendas
    func getAllStores(completionHandler: @escaping ([Store]) -> Void){
        self.ref.child(EntitiesNameFireBase.Store).observeSingleEvent(of: .value) { (snapshot, textOptional) in
            guard let value = snapshot.value else {
                completionHandler([Store]())
                return
            }
            do {
                let model = try FirebaseDecoder().decode([Store].self, from: value)
                completionHandler(model)
            } catch let error {
                print(error)
                completionHandler([Store]())
            }
        }
    }
    
    //Function para devolver todos los productos de una tienda con disponibilidad en el inventario
    func getAllProductsForStore(store: Store?, completionHandler: @escaping ([Product]) -> Void){
        self.ref.child(EntitiesNameFireBase.Inventory)
            //.queryOrdered(byChild: "idWarehouse")
            //.queryEqual(toValue: store.idWarehouse)
            .observeSingleEvent(of: .value) { (snapshot, textOptional) in
            guard let value = snapshot.value else {
                return
            }
            do {
                print(value)
                var inventories = try FirebaseDecoder().decode([Inventory].self, from: value)
                self.ref.child(EntitiesNameFireBase.Product).observeSingleEvent(of: .value) { (snapshot, textOptional) in
                    guard let value = snapshot.value else {
                        completionHandler([Product]())
                        return
                    }
                    do {
                        let products = try FirebaseDecoder().decode([Product].self, from: value)
                        var productsFilter = [Product]()
                        if products.count > 0 && inventories.count > 0 {
                            for product in products {
                                var indexInventories = 0
                                if inventories.count > 0 {
                                    for inventory in inventories {
                                        if inventory.idProduct == product.id || ( store != nil && inventory.idWarehouse == store!.idWarehouse && inventory.idProduct == product.id) {
                                            product.stock = inventory.quantity
                                            product.quantity = self.getQuantityProductSaved(id: product.id!)
                                            inventories.remove(at: indexInventories)
                                            productsFilter.append(product)
                                            break
                                        }
                                        indexInventories = indexInventories + 1
                                    }
                                }
                            }
                        }
                        completionHandler(productsFilter)
                    } catch let error {
                        print(error)
                        completionHandler([Product]())
                    }
                }
            } catch let error {
                print(error)
                completionHandler([Product]())
            }
        }
    }
    
    private func getQuantityProductSaved(id: String) -> Int {
        if let productsPurchased = JSONManager().getCarToBuySaved() {
            if productsPurchased.count > 0 {
              let productsResult =  productsPurchased.filter { (productPurchase) -> Bool in
                    if productPurchase.idProduct == id {
                        return true
                    }
                    return false
                }
                if productsResult.count > 0 {
                    return productsResult[0].quantity!
                }
            }
        }
        return 0
    }
    
    //Se debe gestionar un servicio que nos permita crear la orden de factura
    func getNewOrderPurchase() -> String{
        return "123"
    }
    
    //Funcion para devolver el inventario de los productos que se requieren de la bodega que tiene asociada la tienda
    func getInventoryForProducts(products: [Product], store: Store,  completionHandler: @escaping ([Inventory]) -> Void){
        self.ref.child(EntitiesNameFireBase.Inventory).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value else {
                completionHandler([Inventory]())
                return
            }
            do {
                let model = try FirebaseDecoder().decode([Inventory].self, from: value)
                completionHandler(model)
            } catch let error {
                print(error)
                completionHandler([Inventory]())
            }
        }
    }
    
    func getInventoryForProduct(product: Product, store: Store, completionHandler: @escaping (Inventory) -> Void) {
        self.ref.child(EntitiesNameFireBase.Inventory).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value else {
                completionHandler(Inventory())
                return
            }
            do {
                let model = try FirebaseDecoder().decode([Inventory].self, from: value)
                let filterData = model.filter { (inventory) -> Bool in
                    if inventory.idProduct == product.id && inventory.idWarehouse == store.idWarehouse {
                        return true
                    }
                    return false
                }
                if filterData.count > 0 {
                    completionHandler(filterData[0])
                }else{
                    completionHandler(Inventory())
                }
            } catch let error {
                print(error)
                completionHandler(Inventory())
            }
        }
    }
    
    func detectChangedInInventory(products: [Product], store: Store?, completionHandler: @escaping (Inventory?) -> Void){
        self.ref.child(EntitiesNameFireBase.Inventory).removeAllObservers()
        self.ref.child(EntitiesNameFireBase.Inventory).observe(.childChanged) { (snapshot) in
            guard let value = snapshot.value else {
                completionHandler(nil)
                return
            }
            do {
                let model = try FirebaseDecoder().decode(Inventory.self, from: value)
                if store == nil || model.idWarehouse == store!.idWarehouse{
                     completionHandler(model)
                }else{
                    completionHandler(nil)
                }
            } catch let error {
                print(error)
                completionHandler(nil)
            }
        }
    }
    
    //Funcion para comprar productos 
    func buyProducts(products : [Inventory]){
        
    }
    
    //Funcion para crear un nuevo usuario
    func createUser(user: User, completionHandler: @escaping (Bool) -> Void){
        do {
            let data = try FirebaseEncoder().encode(user)
            self.ref.child(EntitiesNameFireBase.Customer).setValue(data){
                (error:Error?, ref:DatabaseReference) in
                if error != nil {
                  completionHandler(true)
                } else {
                  completionHandler(false)
                }
            }
        }catch{
            
        }
    }
    
    
}
