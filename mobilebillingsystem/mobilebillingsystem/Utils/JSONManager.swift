//
//  JSONManager.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 23/02/20.
//  Copyright © 2020 Sebastian Panesso & Jose Avalos. All rights reserved.
//

import Foundation

class JSONManager {
    
    init() {}
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func saveJSONString(key:String, json: String){
        UserDefaults.standard.set(json, forKey: key)
    }

    func getSavedJSONString(key : String) -> String {
        if let json = UserDefaults.standard.string(forKey: key){
            return json
        }else{
            return ""
        }
    }
    
    func getUserSession() -> User? {
        let userJsonString = getSavedJSONString(key: Constants.userDefault.userSession)
        if !userJsonString.isEmpty {
            let jsonData = Data(userJsonString.utf8)
            let decoder = JSONDecoder()
            do {
                let user = try decoder.decode(User.self, from: jsonData)
                return user
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func getCarToBuySaved() -> [ProductPurchased]? {
        let orderPurchasingString = getSavedJSONString(key: Constants.userDefault.orderPurchasing)
        if !orderPurchasingString.isEmpty {
            let jsonData = Data(orderPurchasingString.utf8)
            let decoder = JSONDecoder()
            do {
                let productPurchased = try decoder.decode([ProductPurchased].self, from: jsonData)
                return productPurchased
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func saveDictionary(dict: [String : Any], key: String)->Bool{
        var isDictionarySaved = false
        if let jSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            if let jsonFormated =  String(data: jSONData,encoding: .utf8){
                saveJSONString(key: key, json: jsonFormated)
                isDictionarySaved = true
            }
        }
        return isDictionarySaved
    }
}

