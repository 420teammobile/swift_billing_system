//
//  Constants.swift
//  mobilebillingsystem
//
//  Created by JoseDanielAvalos on 22/02/20.
//  Copyright © 2020 Sebastian Panesso. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct StoryBoardName {
        static let registerController = "Register"
        static let carToBuyController = "CarToBuy"
        static let homeController = "Home"
        static let listProductsController = "ListProducts"
        static let storeAvailablesController = "StoreAvailables"
        static let loginController = "Login"
        static let accountController = "Account"
    }
    
    struct StoryBoardControllerName {
        static let registerController = "RegisterController"
        static let carToBuyController = "CarToBuyController"
        static let homeController = "HomeController"
        static let listProductsController = "ListProductsController"
        static let storeAvailablesController = "StoreAvailablesController"
        static let loginController = "LoginController"
        static let accountController = "AccountController"
    }
    
    struct colors {
        static let principalColor = UIColor(displayP3Red: 36, green: 84, blue: 210, alpha: 1)
        static let gray = UIColor(displayP3Red: 12, green: 12, blue: 12, alpha: 1)
        static let white = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 1)
    }
    
    struct styles {
        static let whiteBarStyle : UIBarStyle  = .black
        static let contentInsetLeft : CGFloat  = 25
        static let contentInsetRight : CGFloat  = 25
        static let contentInsetTop : CGFloat  = 25
        static let contentInsetBottom : CGFloat  = 25
    }
    
    struct userDefault {
        static let userSession = "userSession"
        static let orderPurchasing = "orderPurchasing"
    }
}


